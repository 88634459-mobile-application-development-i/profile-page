import 'package:flutter/material.dart';

String app_theme = "light";

void main() {
  runApp(contactProfilePage());
}

class MyAppTheme {
  static ThemeData appThemeLight() {
    return ThemeData(
      brightness: Brightness.light,
      appBarTheme: AppBarTheme(
        color: Colors.white,
        iconTheme: const IconThemeData(
            color: Colors.cyan,
        ),
      ),
      listTileTheme: ListTileThemeData(
          iconColor: Colors.cyan,
      ),
      iconTheme: const IconThemeData(
          color: Colors.cyan
      ),
    );
  }

  static ThemeData appThemeDark() {
    return ThemeData(
      brightness: Brightness.dark,
      appBarTheme: AppBarTheme(
        color: Color.fromRGBO(48, 48, 48, 1),
        iconTheme: const IconThemeData(
            color: Colors.cyanAccent,
        ),
      ),
      listTileTheme: ListTileThemeData(
        iconColor: Colors.cyanAccent,
      ),
      iconTheme: const IconThemeData(
          color: Colors.cyanAccent
      ),
    );
  }
}

class contactProfilePage extends StatefulWidget {
  @override
  State<contactProfilePage> createState() => _contactProfilePageState();
}

class _contactProfilePageState extends State<contactProfilePage> {
  var currentTheme = app_theme;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme : currentTheme == "dark" ? MyAppTheme.appThemeDark() : MyAppTheme.appThemeLight(),
      home: Scaffold(
        appBar: buildAppBarWidget(),
        body: buildBodyWidget(),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            setState(() {
              currentTheme = currentTheme == "dark" ? "light" : "dark";
            });
          },
          child : Icon(Icons.refresh),
          ),
        ),
      );

  }
}

Widget buildButton(iconName, iconText) {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          iconName,
        ),
        onPressed: () {},
      ),
      Text(iconText),
    ],
  );
}

Widget buildListTile(
    leadingIconName, titleText, subTitleText, trailingIconName) {
  return ListTile(
      leading: Icon(leadingIconName),
      title: Text(titleText),
      subtitle: Text(subTitleText),
      trailing: IconButton(
        icon: Icon(trailingIconName),
        onPressed: () {},
      ));
}

Widget buildListTileNoLeading(titleText, subTitleText, trailingIconName) {
  return ListTile(
      leading: const Text(""),
      title: Text(titleText),
      subtitle: Text(subTitleText),
      trailing: IconButton(
        icon: Icon(trailingIconName),
        onPressed: () {},
      ));
}

Widget buildListTileNoTrailing(
    leadingIconName, titleText, subTitleText) {
  return ListTile(
      leading: Icon(leadingIconName),
      title: Text(titleText),
      subtitle: Text(subTitleText),
      trailing: const Text(""));
}

PreferredSizeWidget buildAppBarWidget() {
  return AppBar(
    leading: const Icon(Icons.arrow_back),
    actions: <Widget>[
      IconButton(
          onPressed: () {}, icon: const Icon(Icons.star_border_outlined)),
    ],
  );
}

ListView buildBodyWidget() {
  return ListView(
    children: <Widget>[
      Column(
        children: <Widget>[
          SizedBox(
            width: double.infinity,

            //Height constraint at Container widget level
            height: 250,

            child: Image.network(
              "https://scontent.fbkk10-1.fna.fbcdn.net/v/t39.30808-6/271587952_1107684330000874_3523023110097328385_n.jpg?_nc_cat=101&ccb=1-7&_nc_sid=09cbfe&_nc_aid=0&_nc_eui2=AeETw2BMHGJhQiovQGoRyVz7u25rjZiZBGO7bmuNmJkEY6y_uk47XFTTpZw6HDisYYejEm345Pn0hdrQ5CFmElbY&_nc_ohc=cE1JzG7-hbYAX8OvnlL&_nc_ht=scontent.fbkk10-1.fna&oh=00_AfAwv9IlxlJ_PHAU15xZtRDtkSggl1wUc_UKhi5Bu23XCg&oe=63A83B09",
              fit: BoxFit.cover,
            ),
          ),
          const Divider(
            thickness: 2,
          ),
          SizedBox(
            height: 60,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: const <Widget>[
                Padding(
                    padding: EdgeInsets.only(left: 10),
                    child: Text("Korrawit Sasiraborvornpat",
                        style: TextStyle(fontSize: 30))),
              ],
            ),
          ),
          const Divider(
            thickness: 2,
          ),
          Container(
            margin: const EdgeInsets.only(top: 8, bottom: 8),
            child : Theme(
              data : ThemeData(
              iconTheme: const IconThemeData(
                color: Colors.cyan
              ),
              ),
            child: profileActionItems()
          ),
          ),
          const Divider(
            thickness: 2,
          ),
          Column(
            children: <Widget>[
              buildListTile(
                  Icons.call, "094-339-XXXX", "mobile", Icons.message),
              buildListTileNoTrailing(Icons.email, "seth8806@gmail.com", "personal"
              ),
              buildListTile(Icons.location_on,
                  "77th St, Los Angeles", "home", Icons.directions),
            ],
          )
        ],
      )
    ],
  );
}

Widget profileActionItems() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: <Widget>[
      buildButton(Icons.call, "Call"),
      buildButton(Icons.message, "Text"),
      buildButton(Icons.video_call, "Video"),
      buildButton(Icons.email, "Email"),
      buildButton(Icons.directions, "Directions"),
      buildButton(Icons.attach_money, "Pay"),
    ],
  );
}

